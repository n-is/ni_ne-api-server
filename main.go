package main

import (
	"nine/src/api"
	"nine/src/config"
)

func main() {
	config.Load()
	api.Run()
}
