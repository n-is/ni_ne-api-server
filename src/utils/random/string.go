package random

import (
	"math/rand"
	"time"
)

var letters = []rune(`abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ;:~!@#$^&*(){}[]<>,.?\\|/"'_+=-0987654321`)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func String(minL, maxL int) string {
	if minL > maxL {
		tmp := maxL
		maxL = minL
		minL = tmp
	}
	randLen := minL + rand.Intn(maxL-minL)
	return randSeq(randLen)
}
