package logger

import (
	"fmt"
	"log"
	"strings"
)

func Error(strs ...interface{}) {
	log.Printf("[ERROR] %v", getStr(" ", strs))
}

func Warn(strs ...interface{}) {
	log.Printf("[WARN] %v", getStr(" ", strs))
}

func Info(strs ...interface{}) {
	log.Printf("[INFO] %v", getStr(" ", strs))
}

func getStr(sep string, v ...interface{}) string {
	if len(v) == 0 {
		return ""
	}

	var str strings.Builder

	if len(v) == 1 {
		str.WriteString(fmt.Sprintf("%v", v[0]))
		return str.String()
	}

	for _, val := range v {
		str.WriteString(sep)
		str.WriteString(fmt.Sprintf("%v", val))
	}

	return str.String()
}
