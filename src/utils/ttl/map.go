package ttl

import (
	"log"
	"sync"
	"time"
)

type item struct {
	value      interface{}
	insertTime time.Time
}

type Map struct {
	mMu sync.Mutex
	m   map[string]*item
}

func NewMap(ttl, tick time.Duration) (m *Map) {
	m = &Map{
		m: make(map[string]*item),
	}

	go func() {
		for range time.Tick(tick) {
			m.mMu.Lock()
			for k, v := range m.m {
				since := time.Since(v.insertTime)
				if since >= ttl {
					log.Println("Deleting Key:", k)
					log.Println("Key Kept For:", since)
					delete(m.m, k)
				}
			}
			m.mMu.Unlock()
		}
	}()
	return
}

func (m *Map) Delete(k string) bool {
	m.mMu.Lock()
	defer m.mMu.Unlock()
	if _, ok := m.m[k]; ok {
		delete(m.m, k)
		return true
	}
	return false
}

func (m *Map) Len() int {
	return len(m.m)
}

func (m *Map) Insert(k string, v interface{}) {
	m.mMu.Lock()
	it := &item{value: v}
	m.m[k] = it
	it.insertTime = time.Now()
	m.mMu.Unlock()
}

func (m *Map) Get(k string) (interface{}, bool) {
	m.mMu.Lock()
	defer m.mMu.Unlock()
	if it, ok := m.m[k]; ok {
		return it.value, true
	}
	return nil, false
}
