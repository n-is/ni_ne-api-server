package ttl

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestTTLMap(t *testing.T) {
	m := NewMap(1 * time.Second, 500 * time.Millisecond)

	t.Run("Test 1", func(t *testing.T) {
		m.Insert("item", true)
		time.Sleep(300 * time.Millisecond)
		if _, ok := m.Get("item"); !ok {
			t.Error("Item should not have been deleted")
		}
		assert.Equal(t, 1, m.Len())

		time.Sleep(1 * time.Second)

		if _, ok := m.Get("item"); ok {
			t.Error("Item should have been deleted")
		}
		assert.Equal(t, 0, m.Len())
	})

	t.Run("Test 2", func(t *testing.T) {
		m.Insert("item", true)
		time.Sleep(300 * time.Millisecond)
		if _, ok := m.Get("item"); !ok {
			t.Error("Item should not have been deleted")
		}

		m.Delete("item")
		if _, ok := m.Get("item"); ok {
			t.Error("Item should have been deleted")
		}
	})
}
