package config

import (
	"fmt"
	"net/smtp"
	"nine/src/utils/ttl"
	"os"
	"strconv"
	"time"
)

var (
	ApiPort   = 5000
	SecretKey []byte
	DbDriver  = ""
	DbURL     = ""

	ServerMail = ""
	PlainSMTPAuth smtp.Auth

	TokenExpDuration time.Duration
	MailTokenExpDuration time.Duration

	MailTokenBucket *ttl.Map

)

func Load() {
	var err error
	//if err = godotenv.Load(); err != nil {
	//	log.Panic("Failed to load environment variables")
	//}

	ApiPort, err = strconv.Atoi(os.Getenv("PORT"))
	if err != nil {
		ApiPort = 5000
	}

	exp, err := strconv.Atoi(os.Getenv("TOKEN_EXP_DURATION"))
	if err != nil {
		exp = 60
	}
	TokenExpDuration = time.Duration(exp) * time.Minute

	exp, err = strconv.Atoi(os.Getenv("MAIL_TOKEN_EXP_DURATION"))
	if err != nil {
		exp = 10
	}
	MailTokenExpDuration = time.Duration(exp) * time.Minute

	DbDriver = os.Getenv("DB_DRIVER")
	DbURL = fmt.Sprintf("%s:%s@(%s)/%s?charset=utf8&parseTime=True&loc=Local",
		os.Getenv("DB_USER"), os.Getenv("DB_PASS"), os.Getenv("DB_HOST"), os.Getenv("DB_NAME"))

	SecretKey = []byte(os.Getenv("API_SECRET"))

	ServerMail = os.Getenv("SERVER_MAIL")
	PlainSMTPAuth = smtp.PlainAuth("", ServerMail, os.Getenv("SERVER_MAIL_PASSWORD"), "smtp.gmail.com")

	MailTokenBucket = ttl.NewMap(MailTokenExpDuration, 2*time.Minute)
}
