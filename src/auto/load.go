package auto

import (
	"log"
	"nine/src/api/database"
	"nine/src/models"
)

func Load() {
	db, err := database.Connect()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	err = db.Debug().DropTableIfExists(&models.User{}, &models.Profile{}).Error
	if err != nil {
		log.Panic(err)
	}

	err = db.Debug().AutoMigrate(&models.Profile{}, &models.User{}).Error
	if err != nil {
		log.Panic(err)
	}

	for i := range sampleUsers {
		err = db.Debug().Model(&models.User{}).Create(&sampleUsers[i]).Error
		if err != nil {
			log.Panic(err)
		}

		err = db.Debug().Model(&models.Profile{}).Create(&sampleProfiles[i]).Error
		if err != nil {
			log.Panic(err)
		}
	}
}
