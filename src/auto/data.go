package auto

import "nine/src/models"

var User1 = models.User{
	Email:    "mary@example.com",
	Password: "mary_had_a_little_lamb",
}

var User2 = models.User{
	Email:    "lamb@example.com",
	Password: "lamb_is_tasty",
}

var user1Name = "Mary Bell"
var user1Phone = "(+977) 1234567890"
var user1Address = "Kathmandu, Nepal"

var user2Name = "Mr. Lamb"
var user2Phone = "(+977) 2143224411"
var user2Address = "Itahari, Nepal"

var User1Profile = models.Profile{
	Name:    &user1Name,
	Phone:   &user1Phone,
	Gender:  models.FEMALE,
	Address: &user1Address,
}

var User2Profile = models.Profile{
	Name:    &user2Name,
	Phone:   &user2Phone,
	Gender:  models.MALE,
	Address: &user2Address,
}

var sampleUsers = []models.User{User1, User2}
var sampleProfiles = []models.Profile{User1Profile, User2Profile}
