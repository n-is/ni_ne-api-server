package jwtok

import (
	"errors"
	"fmt"
	"net/http"
	"nine/src/config"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

func CreateToken(userID uint32, duration time.Duration) (string, error) {
	claims := jwt.MapClaims{}

	claims["authorized"] = true
	claims["user_id"] = userID
	claims["exp"] = time.Now().Add(duration).Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(config.SecretKey)
}

func TokenValid(tokenString string) error {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return config.SecretKey, nil
	})
	if err != nil {
		return err
	}

	if _, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return nil
	}
	return errors.New("invalid token")
}

func ExtractToken(r *http.Request) (string, error) {
	keys := r.URL.Query()
	token := keys.Get("token")
	if token != "" {
		return token, nil
	}

	bearerToken := r.Header.Get("Authorization")
	tokens := strings.Fields(bearerToken)
	if len(tokens) == 2 {
		return tokens[1], nil
	}

	return "", errors.New("missing token")
}

func ExtractTokenID(r *http.Request) (uint32, error) {
	tokenString, err := ExtractToken(r)
	if err != nil {
		return 0, err
	}

	return IdFromToken(tokenString)
}

func IdFromToken(tokenString string) (uint32, error) {

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return config.SecretKey, nil
	})
	if err != nil {
		return 0, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		uid, err := strconv.ParseUint(fmt.Sprintf("%v", claims["user_id"]), 10, 32)
		if err != nil {
			return 0, err
		}
		return uint32(uid), nil
	}
	return 0, errors.New("invalid token")
}
