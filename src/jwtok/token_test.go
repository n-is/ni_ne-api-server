package jwtok

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestToken(t *testing.T) {
	ids := []uint32{
		1, 2, 789, 324, 14325, 0, 214325,
	}

	for _, id := range ids {
		tok, err := CreateToken(id, 10*time.Minute)
		assert.Nil(t, err)

		exId, err := IdFromToken(tok)
		assert.Nil(t, err)

		assert.Equal(t, id, exId)
	}
}
