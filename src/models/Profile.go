package models

import (
	"errors"
	"fmt"
	"regexp"
	"time"
)

const (
	FEMALE    = 1
	MALE      = 2
	NOTBINARY = 3
)

var profileValidator map[string]*regexp.Regexp

func init() {
	profileValidator = make(map[string]*regexp.Regexp)
	profileValidator["phone"] = regexp.MustCompile(`^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d+\)?[\-\.\ \\\/]?)*)(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$`)
}

type Profile struct {
	ID   uint32 `json:"id"`
	User User   `gorm:"foreignkey:ID" json:"-"`

	Name    *string `gorm:"size:254" json:"name"`
	Phone   *string `gorm:"size:20" json:"phone"`
	Gender  uint8   `gorm:"not null" json:"gender"`
	Address *string `gorm:"size:254" json:"address"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (p *Profile) BeforeSave() error {

	if err := p.Validate(); err != nil {
		return err
	}

	// Update Timestamps
	p.CreatedAt = time.Now()
	p.UpdatedAt = time.Now()

	return nil
}

func (p *Profile) Validate() error {

	if p.Phone != nil {
		if !profileValidator["phone"].MatchString(*p.Phone) {
			return errors.New(fmt.Sprintf("invalid phone"))
		}
	}

	return nil
}
