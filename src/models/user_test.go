package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEmailValidator(t *testing.T) {
	validEmails := []string{
		"email@example.com",
		"firstname.lastname@example.com",
		"email@subdomain.example.com",
		"firstname+lastname@example.com",
		"email@[123.123.123.123]",
		`“email”@example.com`,
		"1234567890@example.com",
		"email@example-one.com",
		"_______@example.com",
		"email@example.name",
		"email@example.museum",
		"email@example.co.jp",
		"firstname-lastname@example.com",
	}

	for _, email := range validEmails {
		assert.True(t, userValidator["email"].MatchString(email), email)
	}

	invalidEmails := []string{
		`plain-address`,
		`#@%^%#$@#$@#.com`,
		`@example.com`,
		`Joe Smith <email@example.com>`,
		`email.example.com`,
		`email@example@example.com`,
		`.email@example.com`,
		`email.@example.com`,
		`email..email@example.com`,
		`email@example.com (Joe Smith)`,
		`email@example`,
		`email@111.222.333.44444`,
		`email@example..com`,
		`Abc..123@example.com`,
	}

	for _, email := range invalidEmails {
		assert.False(t, userValidator["email"].MatchString(email), email)
	}
}
