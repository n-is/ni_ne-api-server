package models

import (
	"errors"
	"fmt"
	"log"
	"nine/src/api/security"
	"regexp"
	"time"
)

var userValidator map[string]*regexp.Regexp

func init() {
	userValidator = make(map[string]*regexp.Regexp)
	userValidator["email"] = regexp.MustCompile(`^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$`)
}

type User struct {
	ID       uint32 `gorm:"primary_key;auto_increment" json:"id"`
	Email    string `gorm:"size:254;not null;unique" json:"email"`
	Password string `gorm:"size:63;not null" json:"password,omitempty"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (u *User) BeforeSave() error {

	// Hash the password before saving it
	hashedPass, err := security.Hash(u.Password)
	if err != nil {
		return err
	}

	u.Password = string(hashedPass)

	// Update Timestamps
	u.CreatedAt = time.Now()
	u.UpdatedAt = time.Now()

	return nil
}

func ValidateEmail(email string) error {
	if !userValidator["email"].MatchString(email) {
		log.Println("email", email)
		return errors.New(fmt.Sprintf("invalid email"))
	}
	return nil
}
