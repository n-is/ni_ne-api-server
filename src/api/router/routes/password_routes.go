package routes

import (
	"net/http"
	"nine/src/api/controllers"
)

var passwordRoutes = []Route{
	{
		Uri:          "/password/forgot",
		Method:       http.MethodPost,
		Handler:      controllers.ForgotPassword,
		AuthRequired: false,
	},
	{
		Uri:          "/password/reset",
		Method:       http.MethodPost,
		Handler:      controllers.ResetPassword,
		AuthRequired: true,
	},
}
