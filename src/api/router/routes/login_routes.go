package routes

import (
	"net/http"
	"nine/src/api/controllers"
)

var loginRoutes = []Route{
	{
		Uri:          "/login",
		Method:       http.MethodPost,
		Handler:      controllers.Login,
		AuthRequired: false,
	},
}
