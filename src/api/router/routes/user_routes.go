package routes

import (
	"net/http"
	"nine/src/api/controllers"
)

var usersRoutes = []Route{
	{
		Uri:          "/users",
		Method:       http.MethodPost,
		Handler:      controllers.CreateUser,
		AuthRequired: false,
	},
	{
		Uri:          "/users",
		Method:       http.MethodGet,
		Handler:      controllers.GetUser,
		AuthRequired: true,
	},
	{
		Uri:          "/users/email/{email}",
		Method:       http.MethodGet,
		Handler:      controllers.GetUserByEmail,
		AuthRequired: false,
	},
	{
		Uri:          "/users",
		Method:       http.MethodPut,
		Handler:      controllers.UpdateUser,
		AuthRequired: true,
	},
	{
		Uri:          "/users",
		Method:       http.MethodDelete,
		Handler:      controllers.DeleteUser,
		AuthRequired: true,
	},
}
