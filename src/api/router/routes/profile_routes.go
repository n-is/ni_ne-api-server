package routes

import (
	"net/http"
	"nine/src/api/controllers"
)

var profilesRoutes = []Route{
	{
		Uri:          "/users/profile",
		Method:       http.MethodPost,
		Handler:      controllers.CreateProfile,
		AuthRequired: true,
	},
	{
		Uri:          "/users/profile",
		Method:       http.MethodGet,
		Handler:      controllers.GetProfile,
		AuthRequired: true,
	},
	{
		Uri:          "/users/profile",
		Method:       http.MethodPut,
		Handler:      controllers.UpdateProfile,
		AuthRequired: true,
	},
	{
		Uri:          "/users/profile",
		Method:       http.MethodDelete,
		Handler:      controllers.DeleteProfile,
		AuthRequired: true,
	},
}
