package api

import (
	"fmt"
	"log"
	"net/http"

	"nine/src/api/router"
	"nine/src/auto"
	"nine/src/config"
)

func Run() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	config.Load()
	auto.Load()

	fmt.Println("Listening...:", config.ApiPort)
	listen(config.ApiPort)
}

func listen(port int) {
	r := router.New()
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), r))
}
