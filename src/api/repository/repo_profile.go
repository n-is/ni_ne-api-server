package repository

import "nine/src/models"

type ProfileRepo interface {
	Save(profile models.Profile, uid uint32) (models.Profile, error)

	FindById(uint32) (models.Profile, error)

	Update(uint32, models.Profile) (int64, error)

	Delete(uint32) (int64, error)
}
