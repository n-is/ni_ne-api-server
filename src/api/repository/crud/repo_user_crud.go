package crud

import (
	"errors"
	"time"

	"nine/src/models"
	"nine/src/utils/channels"

	"github.com/jinzhu/gorm"
)

type repoUsersCRUD struct {
	db *gorm.DB
}

func (r *repoUsersCRUD) Save(user models.User) (models.User, error) {
	var err error
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)
		err = r.db.Debug().Model(&models.User{}).Create(&user).Error
		if err != nil {
			ch <- false
			return
		}
		ch <- true
	}(done)

	if channels.OK(done) {
		return user, nil
	}
	return models.User{}, err
}

func (r *repoUsersCRUD) FindById(u uint32) (models.User, error) {
	var err error
	user := models.User{}
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)
		err = r.db.Debug().Model(&models.User{}).Where("id = ?", u).Take(&user).Error
		if err != nil {
			ch <- false
			return
		}
		ch <- true
	}(done)

	if channels.OK(done) {
		return user, nil
	}

	if gorm.IsRecordNotFoundError(err) {
		return models.User{}, errors.New("user not found")
	}
	return models.User{}, err
}

func (r *repoUsersCRUD) FindByEmail(em string) (models.User, error) {
	var err error
	user := models.User{}
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)
		err = r.db.Debug().Model(&models.User{}).Where("email = ?", em).Take(&user).Error
		if err != nil {
			ch <- false
			return
		}
		ch <- true
	}(done)

	if channels.OK(done) {
		return user, nil
	}

	if gorm.IsRecordNotFoundError(err) {
		return models.User{}, errors.New("user not found")
	}
	return models.User{}, err
}

func (r *repoUsersCRUD) Update(u uint32, user models.User) (int64, error) {
	var rs *gorm.DB
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)
		rs = r.db.Debug().Model(&models.User{}).Where("id = ?", u).Update("Email", user.Email)
		r.db.Debug().Model(&models.User{}).Where("id = ?", u).Update("UpdatedAt", time.Now())

		ch <- true
	}(done)

	if channels.OK(done) {
		if rs.Error != nil {
			if gorm.IsRecordNotFoundError(rs.Error) {
				return 0, errors.New("user not found")
			}
			return 0, rs.Error
		}
		return rs.RowsAffected, nil
	}
	return 0, rs.Error
}

func (r *repoUsersCRUD) Delete(u uint32) (int64, error) {
	var rs *gorm.DB
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)
		rs = r.db.Debug().Model(&models.User{}).Where("id = ?", u).Take(&models.User{}).Delete(&models.User{})
		ch <- true
	}(done)

	if channels.OK(done) {
		if rs.Error != nil {
			return 0, nil
		}
		return rs.RowsAffected, nil
	}
	return 0, rs.Error
}

func NewRepoUsersCRUD(db *gorm.DB) *repoUsersCRUD {
	return &repoUsersCRUD{
		db: db,
	}
}
