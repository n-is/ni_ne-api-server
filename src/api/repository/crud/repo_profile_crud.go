package crud

import (
	"errors"
	"time"

	"nine/src/models"
	"nine/src/utils/channels"

	"github.com/jinzhu/gorm"
)

type repoProfilesCRUD struct {
	db *gorm.DB
}

func (r *repoProfilesCRUD) Save(profile models.Profile, uid uint32) (models.Profile, error) {
	var err error
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)

		var user models.User
		err = r.db.Debug().Model(&models.User{}).Where("id = ?", uid).Take(&user).Error
		if err != nil {
			ch <- false
			return
		}

		profile.ID = user.ID
		err = r.db.Debug().Model(&models.Profile{}).Create(&profile).Error
		if err != nil {
			ch <- false
			return
		}

		ch <- true
	}(done)

	if channels.OK(done) {
		return profile, nil
	}
	return models.Profile{}, err
}

func (r *repoProfilesCRUD) FindById(uid uint32) (models.Profile, error) {
	var err error
	profile := models.Profile{}
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)
		err = r.db.Debug().Model(&models.Profile{}).Where("id = ?", uid).Take(&profile).Error
		if err != nil {
			ch <- false
			return
		}
		ch <- true
	}(done)

	if channels.OK(done) {
		return profile, nil
	}

	if gorm.IsRecordNotFoundError(err) {
		return models.Profile{}, errors.New("profile not found")
	}
	return models.Profile{}, err
}

func (r *repoProfilesCRUD) Update(u uint32, profile models.Profile) (int64, error) {
	var rs *gorm.DB
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)

		if profile.Name != nil {
			rs = r.db.Debug().Model(&models.Profile{}).Where("id = ?", u).Update("Name", profile.Name)
		}
		if profile.Address != nil {
			rs = r.db.Debug().Model(&models.Profile{}).Where("id = ?", u).Update("Address", profile.Address)
		}
		if profile.Phone != nil {
			rs = r.db.Debug().Model(&models.Profile{}).Where("id = ?", u).Update("Phone", profile.Phone)
		}
		if profile.Gender != 0 {
			rs = r.db.Debug().Model(&models.Profile{}).Where("id = ?", u).Update("Gender", profile.Gender)
		}
		rs = r.db.Debug().Model(&models.Profile{}).Where("id = ?", u).Update("UpdatedAt", time.Now())

		ch <- true
	}(done)

	if channels.OK(done) {
		if rs.Error != nil {
			return 0, nil
		}
		return rs.RowsAffected, nil
	}
	return 0, rs.Error
}

func (r *repoProfilesCRUD) Delete(u uint32) (int64, error) {
	var rs *gorm.DB
	done := make(chan bool)

	go func(ch chan<- bool) {
		defer close(ch)

		rs = r.db.Debug().Model(&models.Profile{}).Where("id = ?", u).Take(&models.Profile{}).Delete(&models.Profile{})
		ch <- true
	}(done)

	if channels.OK(done) {
		if rs.Error != nil {
			return 0, nil
		}
		return rs.RowsAffected, nil
	}
	return 0, rs.Error
}

func NewRepoProfilesCRUD(db *gorm.DB) *repoProfilesCRUD {
	return &repoProfilesCRUD{
		db: db,
	}
}
