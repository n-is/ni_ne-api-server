package repository

import "nine/src/models"

type UserRepo interface {
	Save(models.User) (models.User, error)

	FindById(uint32) (models.User, error)

	FindByEmail(string) (models.User, error)

	Update(uint32, models.User) (int64, error)

	Delete(uint32) (int64, error)
}
