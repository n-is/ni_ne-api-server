package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"nine/src/api/auth"
	"nine/src/api/database"
	"nine/src/api/repository"
	"nine/src/api/repository/crud"
	"nine/src/jwtok"
	"nine/src/models"
	"nine/src/responses"

	"github.com/gorilla/mux"
)

// CreateUser creates a user and returns back the JWT.
func CreateUser(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	user := models.User{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	if err = models.ValidateEmail(user.Email); err != nil {
		responses.ERROR(w, http.StatusNotAcceptable, err)
		return
	}
	passwd := user.Password

	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	if err := saveUser(db, user); err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
	}
	w.Header().Set("Address", fmt.Sprintf("%s%s/%d", r.Host, r.RequestURI, user.ID))

	token, err := auth.SignIn(user.Email, passwd)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
	}
	responses.JSON(w, http.StatusCreated, map[string]string{
		"token": token,
	})
}

func GetUserByEmail(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	email := vars["email"]

	if err := models.ValidateEmail(email); err != nil {
		responses.ERROR(w, http.StatusNotAcceptable, err)
		return
	}

	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	rep := crud.NewRepoUsersCRUD(db)

	func(userRepo repository.UserRepo) {
		user, err := userRepo.FindByEmail(email)

		if err != nil {
			if err.Error() == "user not found" {
				responses.ERROR(w, http.StatusNotFound, err)
				return
			}

			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}

		// Send Info besides password
		user.Password = ""
		responses.JSON(w, http.StatusOK, user)
	}(rep)
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	uid, err := jwtok.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, err)
		return
	}

	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	rep := crud.NewRepoUsersCRUD(db)

	func(userRepo repository.UserRepo) {
		user, err := userRepo.FindById(uid)

		if err != nil {
			if err.Error() == "user not found" {
				responses.ERROR(w, http.StatusNotFound, err)
				return
			}

			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}

		// Send Info besides password
		user.Password = ""
		responses.JSON(w, http.StatusOK, user)
	}(rep)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	uid, err := jwtok.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, err)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	user := models.User{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	if err := models.ValidateEmail(user.Email); err != nil {
		responses.ERROR(w, http.StatusNotAcceptable, err)
		return
	}

	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	rep := crud.NewRepoUsersCRUD(db)

	func(userRepo repository.UserRepo) {
		row, err := userRepo.Update(uid, user)
		if err != nil {
			if err.Error() == "user not found" {
				responses.ERROR(w, http.StatusNotFound, err)
				return
			}
			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}

		// Send Info besides password
		user.Password = ""
		responses.JSON(w, http.StatusOK, row)
	}(rep)
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	uid, err := jwtok.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, err)
		return
	}

	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	rep := crud.NewRepoUsersCRUD(db)

	func(userRepo repository.UserRepo) {
		_, err := userRepo.Delete(uid)
		if err != nil {
			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}
		w.Header().Set("Entity", fmt.Sprintf("%d", uid))
		responses.JSON(w, http.StatusNoContent, "")
	}(rep)
}
