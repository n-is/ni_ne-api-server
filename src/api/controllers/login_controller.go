package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"nine/src/api/auth"
	"nine/src/models"
	"nine/src/responses"

	"github.com/jinzhu/gorm"
)

func Login(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	user := models.User{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	token, err := auth.SignIn(user.Email, user.Password)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			responses.ERROR(w, http.StatusNotFound, err)
			return
		}
		responses.ERROR(w, http.StatusUnauthorized, err)
		return
	}

	responses.JSON(w, http.StatusOK, map[string]string{
		"token": token,
	})
}
