package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"nine/src/api/database"
	"nine/src/api/repository"
	"nine/src/api/repository/crud"
	"nine/src/jwtok"
	"nine/src/models"
	"nine/src/responses"
)

func CreateProfile(w http.ResponseWriter, r *http.Request) {
	uid, err := jwtok.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, err)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	profile := models.Profile{}
	err = json.Unmarshal(body, &profile)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := crud.NewRepoProfilesCRUD(db)

	func(profileRepo repository.ProfileRepo) {
		profile, err = profileRepo.Save(profile, uid)
		if err != nil {
			responses.ERROR(w, http.StatusUnprocessableEntity, err)
			return
		}
		w.Header().Set("Location", fmt.Sprintf("%s%s/%d", r.Host, r.RequestURI, profile.ID))
		responses.JSON(w, http.StatusCreated, profile)
	}(repo)
}

func GetProfile(w http.ResponseWriter, r *http.Request) {
	uid, err := jwtok.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, err)
		return
	}

	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := crud.NewRepoProfilesCRUD(db)

	func(profileRepo repository.ProfileRepo) {
		profile, err := profileRepo.FindById(uid)
		if err != nil {
			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}
		responses.JSON(w, http.StatusOK, profile)
	}(repo)
}

func UpdateProfile(w http.ResponseWriter, r *http.Request) {
	uid, err := jwtok.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, err)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	profile := models.Profile{}
	err = json.Unmarshal(body, &profile)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	profile.ID = uid

	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := crud.NewRepoProfilesCRUD(db)

	func(profileRepo repository.ProfileRepo) {
		row, err := profileRepo.Update(uid, profile)
		if err != nil {
			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}
		responses.JSON(w, http.StatusOK, row)
	}(repo)
}

func DeleteProfile(w http.ResponseWriter, r *http.Request) {
	uid, err := jwtok.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, err)
		return
	}

	db, err := database.Connect()
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := crud.NewRepoProfilesCRUD(db)

	func(profileRepo repository.ProfileRepo) {
		_, err := profileRepo.Delete(uid)
		if err != nil {
			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}
		w.Header().Set("Entity", fmt.Sprintf("%d", uid))
		responses.JSON(w, http.StatusNoContent, "")
	}(repo)
}
