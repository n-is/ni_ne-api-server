package controllers

import (
	"nine/src/api/repository/crud"
	"nine/src/models"

	"github.com/jinzhu/gorm"
)

func saveUser(db *gorm.DB, user models.User) error {
	var err error
	rep := crud.NewRepoUsersCRUD(db)
	profRep := crud.NewRepoProfilesCRUD(db)

	user, err = rep.Save(user)
	if err != nil {
		return err
	}

	_, err = profRep.Save(models.Profile{}, user.ID)
	if err != nil {
		return err
	}

	return nil
}
