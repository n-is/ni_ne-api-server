package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/smtp"
	"time"

	"nine/src/api/database"
	"nine/src/api/repository/crud"
	"nine/src/api/security"
	"nine/src/config"
	"nine/src/jwtok"
	"nine/src/models"
	"nine/src/responses"

	"github.com/jinzhu/gorm"
)

type passwordOnly struct {
	Password string `json:"password"`
}

func generateToken(er models.EmailRedirectInfo) (string, error) {
	db, err := database.Connect()
	if err != nil {
		return "", err
	}
	defer db.Close()

	repo := crud.NewRepoUsersCRUD(db)
	user, err := repo.FindByEmail(er.Email)
	if err != nil {
		if err.Error() == "user not found" {
			return "", errors.New("no content")
		}
		return "", err
	}

	return jwtok.CreateToken(user.ID, config.MailTokenExpDuration)
}

func generateRedirectURL(redirect, token string) string {
	return fmt.Sprintf("%s?token=%s", redirect, token)
}

func ForgotPassword(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	redirect := models.EmailRedirectInfo{}
	err = json.Unmarshal(body, &redirect)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	token, err := generateToken(redirect)
	if err != nil {
		if err.Error() == "no content" {
			responses.ERROR(w, http.StatusNotFound, err)
			return
		}

		responses.ERROR(w, http.StatusInternalServerError, err)
	}

	redirectURL := generateRedirectURL(redirect.Redirect, token)
	mailBody := fmt.Sprintf(`
To: %s
From: %s
Subject: Password Reset
%s

%s`, redirect.Email, redirect.Sender, redirect.Message, redirectURL)

	// Receiver email address.
	to := []string{redirect.Email}

	// Sending email.
	err = smtp.SendMail("smtp.gmail.com:587", config.PlainSMTPAuth, config.ServerMail, to, []byte(mailBody))
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	fmt.Println("Email Sent Successfully to:", redirect.Email)

	config.MailTokenBucket.Insert(token, true)
	responses.JSON(w, http.StatusOK, redirect)
}

func ResetPassword(w http.ResponseWriter, r *http.Request) {
	// Extract Token
	token, err := jwtok.ExtractToken(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	if _, ok := config.MailTokenBucket.Get(token); ok {
		// Remove the key from the bucket
		config.MailTokenBucket.Delete(token)

		// Extract new password
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			responses.ERROR(w, http.StatusUnprocessableEntity, err)
			return
		}

		password := passwordOnly{}
		err = json.Unmarshal(body, &password)
		if err != nil {
			responses.ERROR(w, http.StatusUnprocessableEntity, err)
			return
		}
		hashedPass, err := security.Hash(password.Password)
		if err != nil {
			responses.ERROR(w, http.StatusUnprocessableEntity, err)
			return
		}

		uid, err := jwtok.IdFromToken(token)
		if err != nil {
			responses.ERROR(w, http.StatusUnprocessableEntity, err)
			return
		}

		db, err := database.Connect()
		if err != nil {
			responses.ERROR(w, http.StatusInternalServerError, err)
			return
		}
		defer db.Close()

		err = db.Debug().Model(&models.User{}).Where("id = ?", uid).Update("Password", string(hashedPass)).Error
		db.Debug().Model(&models.User{}).Where("id = ?", uid).Update("UpdatedAt", time.Now())

		if err != nil {
			if gorm.IsRecordNotFoundError(err) {
				responses.ERROR(w, http.StatusNotFound, err)
				return
			}
			return
		}

		newToken, err := jwtok.CreateToken(uid, config.TokenExpDuration)
		if err != nil {
			responses.ERROR(w, http.StatusInternalServerError, err)
			return
		}

		responses.JSON(w, http.StatusOK, map[string]string{
			"token": newToken,
		})

		return
	}
	responses.ERROR(w, http.StatusNotAcceptable, errors.New("token expired"))
	return
}
