package auth

import (
	"nine/src/api/database"
	"nine/src/api/security"
	"nine/src/config"
	"nine/src/jwtok"
	"nine/src/models"
)

func SignIn(email, password string) (string, error) {
	db, err := database.Connect()
	if err != nil {
		return "", err
	}
	defer db.Close()

	user := models.User{}
	err = db.Debug().Model(&models.User{}).Where("email = ?", email).Take(&user).Error
	if err != nil {
		return "", err
	}

	err = security.VerifyPassword(user.Password, password)
	if err != nil {
		return "", err
	}

	return jwtok.CreateToken(user.ID, config.TokenExpDuration)
}
