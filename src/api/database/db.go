package database

import (
	"nine/src/config"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

func Connect() (*gorm.DB, error) {
	db, err := gorm.Open(config.DbDriver, config.DbURL)
	if err != nil {
		return nil, err
	}
	return db, nil
}
